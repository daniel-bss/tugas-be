## TUGAS RUTIN BACKEND DEVELOPMENT SYNRGY ACADEMY BATCH 4

- [29 Maret](https://gitlab.com/daniel-bss/tugas-be/-/tree/tugas-29-maret) - Algoritma frekuensi karakter, dan Palindrome
- [30 Maret](https://gitlab.com/daniel-bss/tugas-be/-/tree/tugas-30-maret) - Algoritma Prime Number Checking, dan membuat Segitiga
- [1 April](https://gitlab.com/daniel-bss/tugas-be/-/tree/tugas-1-april) - OOP (Abstraction, Inheritance, toString() Overriding, etc)
- [4 April](https://gitlab.com/daniel-bss/tugas-be/-/tree/Challenge_2) - Read-Write Files and calculating Mode, Mean, Median
- [16 Mei](https://gitlab.com/daniel-bss/tugas-be/-/tree/tugas-16-mei) - Membuat table "users" dan "roles" dengan relasi Many To Many 
